# 关于Jogit

Jogit 一款轻量级的git服务器端程序，基于Gogs修改而来，将持续修改UI以优化视觉，提供便捷自助搭建Git服务。
本仓目前只提供UI模板及配置相关文件，完整带后端代码请到Gogs官方下载。

## UI优化记录

1. 首页简化，调整为适合团队介绍的模式
2. 页面主宽度调整为1200px

【预览】
 ![](附图/1.png) 
 
## 主要特性

- 控制面板、用户页面以及活动时间线
- 通过 SSH、HTTP 和 HTTPS 协议操作仓库
- 管理用户、组织和仓库
- 仓库和组织级 Webhook，包括 Slack、Discord 和钉钉
- 仓库 Git 钩子、部署密钥和 Git LFS
- 仓库工单（Issue）、合并请求（Pull Request）、Wiki、保护分支和多人协作
- 从其它代码平台迁移和镜像仓库以及 Wiki
- 在线编辑仓库文件和 Wiki
- Jupyter Notebook 和 PDF 的渲染
- 通过 SMTP、LDAP、反向代理、GitHub.com 和 GitHub 企业版进行用户认证
- 开启两步验证（2FA）登录
- 自定义 HTML 模板、静态文件和许多其它组件
- 多样的数据库后端，包括 PostgreSQL、MySQL、SQLite3 和 [TiDB](https://github.com/pingcap/tidb)
- 超过 [31 种语言](https://crowdin.com/project/gogs)的本地化



## 硬件要求

- 最低的系统硬件要求为一个廉价的树莓派
- 如果用于团队项目管理，建议使用 2 核 CPU 及 512MB 内存
- 当团队成员大量增加时，可以考虑添加 CPU 核数，内存占用保持不变

## 浏览器支持

- 请根据 [Semantic UI](https://github.com/Semantic-Org/Semantic-UI#browser-support) 查看具体支持的浏览器版本。
- 官方支持的最小 UI 尺寸为 **1280*800**，UI 不一定会在更小尺寸的设备上被破坏，但我们无法保证且不会修复。

## 安装部署

在安装Jogit之前，请先下载安装Gogs，这里仅以Gogs二进制方式安装为例。

    1. 解压下载的Gogs压缩包。
    2. 使用命令 `cd` 进入到刚刚创建的目录。
    3. 执行命令 `./gogs web`。
    4. Gogs 默认会在端口 `3000` 启动 HTTP 服务，访问 `/install` 以进行初始配置（例如 http://localhost:3000/install ）。

然后，下载本仓custom代码，上传覆盖同名目录下子目录：

    custom
        |-- templates
        |-- public
        |-- config （建议参考修改）


**目前只提供最近发布的Gogs小版本二进制下载。**

所有的版本都支持 **MySQL**、**PostgreSQL** 和 **TiDB**（使用 MySQL 协议）作为数据库，并且均使用构建标签（build tags）**`cert`** 进行构建。需要注意的是，不同的版本的支持状态有所不同，请根据实际的 Gogs 提示进行操作。

`mws` 表示提供内置 Windows 服务支持，如果您使用 NSSM 请使用另外一个版本。

## 下载Gogs

本页仅展示Gogs最新版的二进制，gogs源码版可以在 [GitHub](https://github.com/gogs) 上查看。

### 0.13.0 @ 2023-02-25

|系统名称|系统类型|SQLite|PAM|下载 ([GitHub](https://github.com/gogs/gogs/releases/tag/v0.13.0))|
|------|----|------|---|--------|
|Linux|386|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_386.zip) \| [TAR.GZ](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_386.tar.gz)|
|Linux|amd64|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_amd64.zip) \| [TAR.GZ](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_amd64.tar.gz)|
|Linux|armv7|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_armv7.zip) \| [TAR.GZ](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_armv7.tar.gz)|
|Linux|armv8|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_armv8.zip) \| [TAR.GZ](https://dl.gogs.io/0.13.0/gogs_0.13.0_linux_armv8.tar.gz)|
|Windows|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_windows_amd64.zip) \| [ZIP w/ mws](https://dl.gogs.io/0.13.0/gogs_0.13.0_windows_amd64_mws.zip)|
|macOS|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_darwin_amd64.zip)|
|macOS|arm64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.13.0/gogs_0.13.0_darwin_arm64.zip)|

### 0.12.11 @ 2023-02-25

|系统名称|系统类型|SQLite|PAM|下载 ([GitHub](https://github.com/gogs/gogs/releases/tag/v0.12.11))|
|------|----|------|---|--------|
|Linux|386|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_386.zip) \| [TAR.GZ](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_386.tar.gz)|
|Linux|amd64|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_amd64.zip) \| [TAR.GZ](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_amd64.tar.gz)|
|Linux|armv7|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_armv7.zip) \| [TAR.GZ](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_armv7.tar.gz)|
|Linux|armv8|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_armv8.zip) \| [TAR.GZ](https://dl.gogs.io/0.12.11/gogs_0.12.11_linux_armv8.tar.gz)|
|Windows|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_windows_amd64.zip) \| [ZIP w/ mws](https://dl.gogs.io/0.12.11/gogs_0.12.11_windows_amd64_mws.zip)|
|macOS|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_darwin_amd64.zip)|
|macOS|arm64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.12.11/gogs_0.12.11_darwin_arm64.zip)|

### 0.11.91 @ 2019-08-11

|系统名称|系统类型|SQLite|PAM|下载（[GitHub](https://github.com/gogs/gogs/releases/tag/v0.11.91)）|
|------|----|------|---|--------|
|Linux|386|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_386.zip) \| [TAR.GZ](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_386.tar.gz)|
|Linux|amd64|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_amd64.zip) \| [TAR.GZ](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_amd64.tar.gz)|
|Linux|armv5|❌|❌|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_armv5.zip)|
|Linux|armv6|❌|❌|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_linux_armv6.zip)|
|Raspberry Pi|v2/v3 / armv7|✅|✅|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_raspi_armv7.zip) \| [TAR.GZ](https://dl.gogs.io/0.11.91/gogs_0.11.91_raspi_armv7.tar.gz)|
|Windows|386|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_windows_386.zip) \| [ZIP w/ mws](https://dl.gogs.io/0.11.91/gogs_0.11.91_windows_386_mws.zip)|
|Windows|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_windows_amd64.zip) \| [ZIP w/ mws](https://dl.gogs.io/0.11.91/gogs_0.11.91_windows_amd64_mws.zip)|
|macOS|amd64|✅|❌|HTTPS: [ZIP](https://dl.gogs.io/0.11.91/gogs_0.11.91_darwin_amd64.zip)|



## 特别鸣谢

- 感谢 [Gogs](https://gogs.io/) 及其所用的各相关开源产品作者 。

## 贡献成员

- 您可以通过gogs相关页面查看 [贡献者页面](https://github.com/gogs/gogs/graphs/contributors) 获取 TOP 100 的贡献者列表。

## 授权许可

本项目采用 MIT 开源授权许可证，完整的授权说明已放置在 [LICENSE](./LICENSE) 文件中。